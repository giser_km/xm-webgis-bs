const { defineConfig } = require('@vue/cli-service')

// The path to the CesiumJS source code
const cesiumSource = 'node_modules/cesium/Source';
const cesiumWorkers = '../Build/Cesium/Workers';
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

process.env.VUE_APP_VERSION = require('./package.json').version;

module.exports = defineConfig({
  transpileDependencies: true,
  configureWebpack: {
    output: {
      sourcePrefix: ''
    },
    resolve: {
      fallback: { "https": false, "zlib": false, "http": false, "url": false },
      mainFiles: ['index', 'Cesium']
    },
    plugins: [
      // Copy Cesium Assets, Widgets, and Workers to a static directory
      new CopyWebpackPlugin({
        patterns: [
          { from: path.join(cesiumSource, cesiumWorkers), to: 'Workers' },
          { from: path.join(cesiumSource, 'Assets'), to: 'Assets' },
          { from: path.join(cesiumSource, 'Widgets'), to: 'Widgets' }
        ]
      }),
      new webpack.DefinePlugin({
        // Define relative base path in cesium for loading assets
        CESIUM_BASE_URL: JSON.stringify('')
      })
    ],
  },
  devServer: {
    proxy: {
      '/api1': { // 请求的代称，写在Axios里的BaseUrl
        target: 'https://www.ceic.ac.cn/ajax', // 真实请求URl   https://www.ceic.ac.cn/ajax/speedsearch?num=1&&page=4
        ws: true,
        changeOrigin: true, // 允许跨域
        pathRewrite: { //替换，通配/api的替换成对应字符
          //     /* 重写路径，当我们在浏览器中看到请求的地址为：http://localhost:8080/api/core/getData/userInfo 时
          //       实际上访问的地址是：http://localhost:8088/spring/core/getData/userInfo,因为重写了 /api
          //      */
          // '^/api1': '' //当你的接口中没有/api字眼时，采用这种，直接替换成空即可
          '^/api1': ''   //当你的接口中刚好有/api 时，采用这种方式
        }
      },
      '/api2': {
        target: 'https://www.baidu.com', 
        ws: true,
        changeOrigin: true, 
        pathRewrite: { 
          '^/api2': ''
        }
      }
    },
    client: {
      overlay: false,
    },
  }
})
