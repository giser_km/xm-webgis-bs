import request from '@/utils/request'


export const loginAPI = ({ name, password }) => {
    return request({
        url: '/user/login',
        method: 'post',
        data: {
            name, password
        }
    })
}


export const getDataAll = () => {
    return request({
        url: "/point",
    })
}

export const getDataByYaer = (start, end) => {
    return request({
        url: "/point/year",
        method: "get",
        params: {
            start,
            end
        }
    })
}

export const getCityNameQuery = (k) => {
    return request({
        url: "/point/getByCityName",
        method: "post",
        data: { k }

    })
}

export const byTimeBucketQuery = (s, e) => {
    return request({
        url: "/point/getByTimeBucket",
        method: "get",
        params: { start: s, end: e }
    })
}

export const excelExport = (p) => {
    // console.log(points);
    return request({
        url: "/point/excelExport",
        method: "post",
        headers: {
            'Content-Type': 'application/json',

        },
        responseType: 'blob',
        data: p
    })
}

export const byGeometryQuery = (type, coordinates, epsg) => {
    return request({
        url: "/point/getByGeometry",
        method: "post",
        data: { type, coordinates, epsg }
    })
}

export const saveGPData = (v) => {
    return request({
        url: '/gp',
        method: "post",
        data: v
    })
}

export const getLastOne = () => {
    return request({
        url: "/point/lastOne",
    })
}

export const getByCity = () => {
    return request({
        url: "/point/getByCity",
    })
}

// 导出一个名为getById的函数，该函数接收一个参数id
export const getById = (id) => {
    // 返回一个请求对象，该请求对象的url属性值为拼接字符串，其中id为参数
    return request({
        url: "/point/id/" + id,
    })
}

export const getByBuffer = (lon, lat, r) => {
    return request({
        url: "/point/byBuffer",
        params: {
            lon, lat, distance: r
        }
    })
}


// 断层数据获取
export const getFaultLines = () => {
    return request({
      url: '/fault_line',
      method: 'get'
    })
}

export const getFLone = (id) =>{
    return request({
      url: '/fault_line/id/' + id,
      method: 'get'
    })
}