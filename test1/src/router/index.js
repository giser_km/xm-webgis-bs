import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import OlView from '../views/OlView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/cesium',
    name: 'home',
    component: HomeView
  },
  {
    path:'/ol',
    name:'openlayers',
    component:OlView
  },{
    path: "/test",
    name: 'test',
    // 懒加载
    component: () => import('../views/olTest.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
