class ColorUtils {

    constructor() {
        this.colors = ["#ea7ccc", "#9a60b4", "#fc8452", "#3ba272", "#73c0de", "#fac858", "#91cc75", "#5470c6"]
    }
    getColor() {
        let index = Math.floor(Math.random() * this.colors.length);
        return this.colors[index];
    }

    // 随机颜色
    getRandomColor() {
        var color = "#";
        // 使用for循环6次，模拟生成6位16进制颜色码
        for (var i = 0; i < 6; i++) {
            // Math.random() * 16 生成一个0到15之间的随机浮点数，
            // 然后使用按位或操作符(|)与0进行运算，将其转换为整数
            // 这样可以确保结果是0到15之间的整数
            // toString(16)将这个整数转换成16进制字符串
            // 将生成的随机16进制字符追加到color字符串中
            color += ((Math.random() * 16) | 0).toString(16);
        }
        return color;
    }

}

export default new ColorUtils();