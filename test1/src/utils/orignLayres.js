import XYZ from "ol/source/XYZ";
import TileLayer from "ol/layer/Tile";
import LayerTile from "ol/layer/Tile";
import { Style, Stroke, Circle as CircleStyle, Fill } from "ol/style";
import Vector from "ol/source/Vector";
import VectorLayer from "ol/layer/Vector";
import GeoJSON from "ol/format/GeoJSON";
import { Feature } from "ol"

const token = "b9031f80391e6b65bd1dd80dcde1b097";
// const token = "bffd0ce8bba01c741131af47c414eafc";
// const token = "cfa8d419cee99810c9987fca7fe904c9";

/**
 * 高德地图图层
*/
export const GMapLayer_1 = new TileLayer({
    title: "高德地图",
    isRemove: true,
    source: new XYZ({
        url: "http://wprd0{1-4}.is.autonavi.com/appmaptile?lang=zh_cn&size=1&style=7&x={x}&y={y}&z={z}",
        wrapX: false,
        //   extent: [68.17665, 7.96553, 97.40256, 35.49401],
    }),
});
/**
 * 高德地图图层——深色
*/
export const GMapLayer_1_s = new TileLayer({
    title: "高德地图",
    isRemove: true,
    className: "baseLayerClass",
    source: new XYZ({
        url: "http://wprd0{1-4}.is.autonavi.com/appmaptile?lang=zh_cn&size=1&style=7&x={x}&y={y}&z={z}",
        wrapX: false,
        //   extent: [68.17665, 7.96553, 97.40256, 35.49401],
    }),
});


let g = new Vector({
    url: "./geoJson/China_line_.json",
    format: new GeoJSON(),
    wrapX: false
})
/**
 * 中国境界矢量图层
 */
export const UMapVector_1 = new VectorLayer({
    name: "geoJson",
    isRemove: false,
    title: "中国境界矢量图层",
    type: "LineString",
    className: "geoJsonLine",
    source: g,
    zIndex: 990,
    style: new Style({
        stroke: new Stroke({
            color: [71, 137, 227, 1],
            width: 4, // 设置描边宽度为 4 像素
        }),
    }),
});

/**
 * 时间线点图层
 */
export const UMapVector_2 = new VectorLayer({
    source: new Vector({
        wrapX: false,
        // features: new Feature()
    }),
    zIndex: 999,
    title: "时间线点图层",
    name: "timelinePointLayer",
    type: "PointCollection",
    isRemove: false,
    style: new Style({
        image: new CircleStyle({
            radius: 4,
            fill: new Fill({
                color: "yellow",
            }),
            stroke: new Stroke({
                color: "black",
                width: 1,
            }),
        }),
    }),
});



/**
 * 天地图矢量图层
 */
export const TMapLayer_1 = new LayerTile({
    name: "天地图矢量图层",
    isRemove: true,
    source: new XYZ({
        url: `http://t4.tianditu.com/DataServer?T=vec_w&tk=${token}&x={x}&y={y}&l={z}`,
        // wrapX: false,
        crossOrigin: "anonymous",
    }),
});
/**
 * 天地图矢量图层——深色
 */
export const TMapLayer_1_s = new LayerTile({
    name: "天地图矢量图层",
    isRemove: true,
    className: "baseLayerClass",
    source: new XYZ({
        url: `http://t4.tianditu.com/DataServer?T=vec_w&tk=${token}&x={x}&y={y}&l={z}`,
        // wrapX: false,
        crossOrigin: "anonymous",
    }),
});
/**
 * 天地图标注图层
 */
export const TMapTitle_1 = new TileLayer({
    name: "标注图层",
    isRemove: true,
    source: new XYZ({
        // wrapX: false,
        url: `http://t4.tianditu.com/DataServer?T=cva_w&tk=${token}&x={x}&y={y}&l={z}`,
        crossOrigin: "anonymous",
    }),
});
/**
 * 天地图标注图层——深色
 */
export const TMapTitle_1_s = new TileLayer({
    name: "标注图层",
    isRemove: true,
    className: "baseLayerClass",
    source: new XYZ({
        // wrapX: false,
        url: `http://t4.tianditu.com/DataServer?T=cva_w&tk=${token}&x={x}&y={y}&l={z}`,
        crossOrigin: "anonymous",
    }),
});

/**
 * 天地图影像图层
 */
export const TMapLayer_2 = new LayerTile({
    name: "天地图影像图层",
    isRemove: true,
    source: new XYZ({
        url: `http://t4.tianditu.com/DataServer?T=img_w&tk=${token}&x={x}&y={y}&l={z}`,
        // wrapX: false,
        crossOrigin: "anonymous",
    })
})

/**
 * 天地图地形图层
 */
export const TMapLayer_3 = new LayerTile({
    name: "天地图地形图层",
    isRemove: true,
    source: new XYZ({
        url: `http://t4.tianditu.com/DataServer?T=ter_w&tk=${token}&x={x}&y={y}&l={z}`,
        // wrapX: false,
        crossOrigin: "anonymous"
    })
})

/**
 * 天地图全球境界图层
 */
export const TMapLayer_4 = new LayerTile({
    name: "天地图全球境界图层",
    isRemove: true,
    source: new XYZ({
        url: `http://t4.tianditu.com/DataServer?T=ibo_w&tk=${token}&x={x}&y={y}&l={z}`,
        // wrapX: false,
        crossOrigin: "anonymous",
    })
})