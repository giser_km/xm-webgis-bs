import Vue from 'vue'
// import createPersistedState from 'vuex-persistedstate'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    point: {},
    points: [],
    goto: false,
    activeMenu: 'M0',
    selectedId: null,
    layeres: {
      al: [], ul: [
        // { "title": "中国境界矢量图层", "name": "geoJson", "uid": "7", "visible": true, "type": "line", "edit": false, "delete": false }
      ]
    },
    selections: [],
    bookmarks: [],
    isPaused: false,
    XScrollElement: null,
  },
  getters: {
    getPoint: state => state.point,
    getGoto: state => state.goto,
    getPoints: state => state.points,
    getActiveMenu: state => state.activeMenu,
    getLayeres: state => state.layeres,
    getSelections: state => state.selections,
    getSelectedId: state => state.selectedId,
    getBookmarks: state => state.bookmarks,
    getIsPaused: state => state.isPaused,
    getXScrollElement: state => state.XScrollElement,
  },
  mutations: {
    setPoint(state, val) {
      state.point = val
    },
    setGoto(state, val) {
      state.goto = val
    },
    setPoints(state, val) {
      state.points = val
    },
    setActiveMenu(state, val) {
      state.activeMenu = val
    },
    setLayeres(state, val) {
      state.layeres = val
    },
    setSelections(state, val) {
      state.selections = val
    },
    setSelectedId(state, val) {
      state.selectedId = val
    },
    setBookmarks(state, val) {
      state.bookmarks = val
    },
    setIsPaused(state, val) {
      state.isPaused = val
    },
    setXScrollElement(state, val) {
      state.XScrollElement = val
    }
  },
  actions: {
  },
  modules: {
  },
  // plugins: [createPersistedState()]
})
