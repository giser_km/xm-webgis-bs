// 3857坐标系转4326坐标系
// 经度= 平面坐标x/20037508.34*180
// 纬度= 180/(PI*(2*atan(exp(平面坐标y/20037508.34*180*PI/180))-PI/2)


export function toLonLat(x, y) {
    let lon = (x / 20037508.34) * 180
    let lat = (y / 20037508.34) * 180
    lat = (180 / Math.PI) * (2 * Math.atan(Math.exp((lat * Math.PI) / 180)) - Math.PI / 2)
    return [lon, lat]
}


// 4326坐标系转3857坐标系
// 平面坐标x = 经度*20037508.34/108 平面坐标y = log（tan（（90+纬度）*PI/360））/（PI/360）*20037508.34/180
export function fromLonLat(lon, lat) {
    let x = (lon * 20037508.34) / 180
    let y = Math.log(Math.tan(((90 + lat) * Math.PI) / 360)) / (Math.PI / 180)
    y = (y * 20037508.34) / 180
    return [x, y]
}

const lev = { 3: [], 4.5: [], 6: [], 7: [], 8: [], }
/**
 * 按不同的等级采选
 */


export function getLevel_(v, level = lev) {
    // let obj = { 3: [], 4.5: [], 6: [], 7: [], 8: [], 8.1: [] }
    let obj = { 'm1': [], 'm2': [], 'm3': [], 'm4': [] }
    let yaer = v.year
    obj.yaer = yaer
    v.children.forEach(e => {
        e.points.forEach(l => {
            if (l.scale < 3) {
                obj['m1'].push(l)
            } else if (l.scale >= 3 && l.scale <= 4.5) {
                obj['m2'].push(l)
            } else if (l.scale > 4.5 && l.scale < 6) {
                obj['m3'].push(l)
            } else {
                obj['m4'].push(l)
            }
        })
    });
    return obj;

}