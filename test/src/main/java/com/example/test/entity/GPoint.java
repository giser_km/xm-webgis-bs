package com.example.test.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.postgis.Geometry;
import org.postgis.Point;

import java.time.LocalDateTime;

@Data
@TableName("global_point")
public class GPoint {

	private Integer id;

	@TableField(exist = false)
	private Double lat;

	@TableField(exist = false)
	private Double lon;

	private String location;

	private String time;

	private Double scale;

	private Double depth;

	private String dId;
}
