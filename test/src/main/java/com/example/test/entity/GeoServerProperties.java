package com.example.test.entity;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "geoserver")
public class GeoServerProperties {
	@Value("${geoserver.url}")
	private String url;
	@Value("${geoserver.username}")
	private String username;
	@Value("${geoserver.password}")
	private String password;

	@Value("${geoserver.workspacename}")
	private String workspaceName;
	@Value("${geoserver.storename}")
	private String storeName;

}
