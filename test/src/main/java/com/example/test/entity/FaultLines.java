package com.example.test.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.example.test.handler.GeometryTypeHandler;
import lombok.Data;
import org.locationtech.jts.geom.Geometry;
import org.postgis.PGgeometry;
import org.postgresql.geometric.PGpoint;

@Data
public class FaultLines {

	private String id;

	private String name;

	private String time;

	private Double length;

//	private double

	@TableField(exist = false)
	private String wkt;
}
