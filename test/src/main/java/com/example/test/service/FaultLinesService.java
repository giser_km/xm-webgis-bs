package com.example.test.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.test.entity.FaultLines;

public interface FaultLinesService extends IService<FaultLines> {
}
