package com.example.test.common;

import com.example.test.entity.GeoServerProperties;
import it.geosolutions.geoserver.rest.GeoServerRESTManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URL;

@Configuration
public class GeoServerConfig {
	@Autowired
	private GeoServerProperties geoServerProperties;
	@Bean
	public GeoServerRESTManager geoServerRESTManagerFactory() {
		try {
			return new GeoServerRESTManager(new URL(geoServerProperties.getUrl()), geoServerProperties.getUsername(),
											geoServerProperties.getPassword());
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
}
