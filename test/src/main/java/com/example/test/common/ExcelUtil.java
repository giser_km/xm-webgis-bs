package com.example.test.common;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.example.test.entity.Point1;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ExcelUtil {

	/**
	 * 将List<Point1>类型的对象写入到Excel中
	 * @param response 响应对象
	 * @param points 要写入的数据列表
	 * @throws IOException 抛出异常当发生I/O错误时
	 */
	public static void writeExcel(HttpServletResponse response, List<Point1> points) throws IOException {
	    // 创建ExcelWriter对象，绑定到response.getOutputStream()
	    ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream()).build();

	    // 创建WriteSheet对象，设置Sheet的索引为0，名称为"sheet"
	    WriteSheet sheet = EasyExcel.writerSheet(0, "sheet").head(Point1.class).build();

	    // 将数据列表points写入到Excel中，绑定到创建的WriteSheet对象
	    excelWriter.write(points, sheet);

	    // 完成写入操作
	    excelWriter.finish();
	}
}
