package com.example.test;

import it.geosolutions.geoserver.rest.GeoServerRESTManager;
import it.geosolutions.geoserver.rest.GeoServerRESTReader;
import it.geosolutions.geoserver.rest.manager.GeoServerRESTStyleManager;
import org.geotools.data.DataStore;
import org.geotools.data.FeatureWriter;
import org.geotools.data.Transaction;
import org.geotools.data.postgis.WKBReader;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geojson.feature.FeatureJSON;
import org.geotools.geojson.geom.GeometryJSON;
import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.CRS;
import org.geotools.referencing.GeodeticCalculator;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.junit.jupiter.api.Test;
import org.locationtech.jts.geom.*;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKBWriter;
import org.locationtech.jts.io.WKTReader;
import org.locationtech.jts.operation.buffer.BufferOp;
import org.opengis.feature.Feature;
import org.opengis.feature.Property;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.geometry.DirectPosition;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

@SpringBootTest
class TestApplicationTests {

	@Test
	void contextLoads() throws ParseException, IOException {

//		WKBReader reader = new WKBReader( );
//		Geometry geometry = reader.read(WKBReader.hexToBytes("0101000020E61000002C39382229FD5D4085716007088C3E40"));
//// 设置保留6位小数，否则GeometryJSON默认保留4位小数
//		GeometryJSON geometryJson = new GeometryJSON(7);
//		String s = geometryJson.toString(geometry);
//		System.out.println(s);
//
////{"type":"Point","coordinates":[119.9556356,30.5469975]}
////EWKB->转geojson丢失信息
//		Geometry read = geometryJson.read("{\"type\":\"Point\",\"coordinates\":[119.9556356,30.5469975]}");
//		System.out.println(read.toString());
//		WKBWriter wkbWriter = new WKBWriter();
//		byte[] write = wkbWriter.write(geometry);
//		String s1 = WKBWriter.toHex(write);
//		System.out.println(s1);



		/**
		 * 坐标转
		 */
//		String source = "EPSG:4326";
//		String target = "EPSG:4326";
//
//		Coordinate coordinate = new Coordinate(30,100);
//
//		GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
//
//
//
//		try {
//			MathTransform mathTransform = CRS.findMathTransform(CRS.decode(source), CRS.decode(target));
//
//			Point point = geometryFactory.createPoint(coordinate);
//
//			try {
//				Point transform1 = (Point) JTS.transform(point, mathTransform);
//				System.out.println(transform1.toString());
//			} catch (TransformException e) {
//				e.printStackTrace();
//			}
//		} catch (FactoryException e) {
//			e.printStackTrace();
//		}


		/**
		 * 缓冲区创建
 		 */

//		GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
//
//
//		Point point = geometryFactory.createPoint(new Coordinate(87, 45));
//
//		BufferOp bufferOp = new BufferOp(point);
//		bufferOp.setEndCapStyle(1);
//		// 米转换度
//		double mi = 1000D;
//		double degree = mi / (2*Math.PI*6371004)*360;
//
//		// 计算
//		Geometry resultGeometry = bufferOp.getResultGeometry(degree);
//		resultGeometry.setSRID(4326);
//		System.out.println(resultGeometry.toString());
//		System.out.println(resultGeometry.getSRID());

		/**
		 * 计算距离
		 */

//		// 84坐标系构造GeodeticCalculator
//		GeodeticCalculator geodeticCalculator = new GeodeticCalculator(DefaultGeographicCRS.WGS84);
//		// 起点经纬度
//		geodeticCalculator.setStartingGeographicPoint(120,0);
//		// 末点经纬度
//		geodeticCalculator.setDestinationGeographicPoint(121,0);
//		// 计算距离，单位：米
//		double orthodromicDistance = geodeticCalculator.getOrthodromicDistance();
//		System.out.println(orthodromicDistance / 1000D);


		/**
		 * geoserevr 测试
		 */


	}

	@Autowired
	private GeoServerRESTManager geoServerRESTManager;

	@Test
	void testPublishTif() throws Exception {
		GeoServerRESTReader geoServerRESTReader = geoServerRESTManager.getReader();

		GeoServerRESTStyleManager styleManager = geoServerRESTManager.getStyleManager();
		boolean b = styleManager.existsStyle("poi");
		System.out.println("style是否存在：" + b);

		String workspace = "ne";
		boolean workspaceNull = geoServerRESTReader.existsWorkspace(workspace);
		System.out.println("workspace是否存在：" + workspaceNull);
	}

	@Test
	void wkt2Geometry() throws ParseException{
		String wkt = "MULTILINESTRING((121.938882 38.005506,121.988748 38.008121,122.029249 38.012961,122.06857 38.024193,122.099825 38.035517,122.152606 38.045483,122.206662 38.052213,122.249838 38.05481,122.307741 38.053982,122.361388 38.045731,122.405285 38.026951,122.448967 38.001765,122.496623 37.975432,122.547235 37.95862,122.613682 37.932989,122.64701 37.923887,122.682912 37.911533))";

		WKTReader wktReader = new WKTReader();
		WKBReader wkbReader = new WKBReader();
		byte[] bytes = WKBReader.hexToBytes("0101000020E610000085EB51B81E555840EC51B81E85AB4340");
		Geometry read = wkbReader.read(bytes);
		Geometry geometry = wktReader.read(wkt);
		geometry.setSRID(4326);
		System.out.println(Arrays.toString(geometry.getCoordinates()));
		System.out.println(geometry.getSRID());

		System.out.println(Arrays.toString(read.getCoordinates()));
		System.out.println(read.getSRID());

		GeometryJSON geometryJSON = new GeometryJSON(6);

		System.out.println(geometryJSON.toString(geometry));

	}

}
