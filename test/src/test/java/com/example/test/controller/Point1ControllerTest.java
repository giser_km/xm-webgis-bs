package com.example.test.controller;

import com.example.test.common.Utils;
import com.example.test.entity.Point1;
import com.example.test.mapper.Point1Mapper;
import com.example.test.service.Point1Service;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@ExtendWith(MockitoExtension.class)
public class Point1ControllerTest {

    @Mock
    private Point1Mapper point1Mapper;

    @InjectMocks
    private Point1Controller point1Controller;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup(WebApplicationContext webApplicationContext) {
        mockMvc = MockMvcBuilders.standaloneSetup(point1Controller).build();
    }

    @Test
    public void testGetByBuffer() throws Exception {
        // Arrange
        Double lon = 78.51;
        Double lat = 41.19;
        Integer distance = 1110000;
        List<Point1> expectedPoints = new ArrayList<>();
        expectedPoints.add(new Point1()); // Add a mock Point1 object to the list
        System.out.println("expectedPoints");
        System.out.println(expectedPoints);
        when(point1Mapper.getByBuffer(lon, lat, distance)).thenReturn(expectedPoints);

        // Act & Assert
        mockMvc.perform(get("/point/byBuffer")
                .param("lon", String.valueOf(lon))
                .param("lat", String.valueOf(lat))
                .param("r", String.valueOf(distance))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("[]")); // Replace with the expected JSON representation of the list

        // Verify that the mapper was called with the correct parameters
        System.out.println(point1Mapper.getByBuffer(lon, lat, distance));
    }
}
